import os
import sys
import json
import requests

M3U_LOCATION = "https://lightiptv.me/playlist/ott/rgcq7kxesj.m3u"

ABSOLUTE_PATH = "/Users/Baki/Desktop/my_tv/bin/iptv/m3u/"

def get_m3u():
	content = requests.get(M3U_LOCATION)
	schema = content.text.splitlines()[0]
	channels = content.text.splitlines()[1:]
	channels_count = int(len(channels) / 3)

	groups = generate_groups(channels)
	channels = generate_channels(channels, groups)
	generate_files(channels)
	output = {}
	output['groups'] = groups['groups']
	output['channels'] = channels['channels']
	file = open(ABSOLUTE_PATH + "channels" + '.json', 'w+')
	file.write(json.dumps(output, indent=4))
	file.close()


def generate_groups(channels):
	groups_json = {}
	groups_json_list = []
	group_counter = 0
	groups = []
	for group in channels:
		if "#EXTGRP:" in group:
			target_group = group.split(':')
			if len(target_group) == 2:
				if target_group[1] not in groups:
					groups.append(target_group[1])
	for group in groups:
		group_counter += 1
		groups_json_list.append({
			"title": group,
			"number": group_counter
		})

	groups_json['groups'] = groups_json_list
	return groups_json

def generate_channels(channels, groups):
	channels_json = {}
	channels_json_list = []
	channel_counter = 0

	for channel in channels:
		channel_counter += 1
		if channel_counter == 1:
			if "#EXTINF:" in channel:
				target_channels = channel.split(':')
				if len(target_channels) == 2:
					target_channel_name = target_channels[1].split(',')
					channel_name = target_channel_name[1]
		elif channel_counter == 2:
			if "#EXTGRP:" in channel:
				target_group = channel.split(':')
				if len(target_group) == 2:
					for group in groups['groups']:
						if group['title'] == str(target_group[1]):
							channel_group_id = group['number']
							channel_group_title = group['title']
		elif channel_counter == 3:
			channel_url = channel
			channel_no = channel.split('/')[-1:][0]
			channel_counter = 0
			channels_json_list.append({
					"channel_no": channel_no,
					"channel_name": channel_name,
					"channel_group_title": channel_group_title,
					"channel_group_id": channel_group_id,
					"channel_url": channel_url
				})
		else:
			pass
	channels_json['channels'] = channels_json_list
	return channels_json

def generate_files(channels):

	for channel in channels['channels']:
		m3u_path = ABSOLUTE_PATH + str(channel['channel_no'])
		if os.path.exists(m3u_path):
			pass
			# print ("exists")
		else:
			os.mkdir(m3u_path)

		file = open(m3u_path + "/" + str(channel['channel_no']) + '.m3u', 'w+')
		content = "#EXTM3U" + "\n"
		content += "#EXTINF:0," + channel['channel_name'] + "\n"
		content += channel['channel_url'] + "\n"
		file.write(content)
		file.close()

if __name__ == '__main__':
	if os.path.exists(ABSOLUTE_PATH):
		print ("exists")
	else:
		print ("Does not exist")
	get_m3u()