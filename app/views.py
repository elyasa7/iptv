from django.shortcuts import render
import os
import sys
import json
import requests
# Create your views here.


def single_channel(request, channel_id):
	print(channel_id)
	args = {}
	args['channel_id'] = channel_id
	return render(request, "channel.html", args)


def main_channels(request):
	args = {}
	args['channels'] = json.loads(open("/Users/Baki/Desktop/my_tv/bin/iptv/m3u/channels.json").read())
	return render(request, "main.html", args)
